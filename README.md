# Slack Messaging Project

The main idea of this project is to contain a slack-messaging app, which main objective is to send a poll link and data to slack users in a certain slack workspace.

# Deployment

The project is a fully dockerized services containing: celery workers and celery beat, django framework as main service, redis as celery broker and postgressql as persistent database.

To run the project you need at least a Docker version 20.10.2, and Docker-compose version 1.27.4.

When the containers are build, several actions take place

1. Migrations files volumed up in local repo are deleted
2. PostgresSQL DB is clear, so migrations take place.
3. A user its created in order to interact with the Django project.
4. Test of the slack_poll app are run and their status prompted to docker-compose up log.
5. In case of the celery containers, their worker are started.
6. In case of the main container "slack_messaging" a Django runserver its exposed on port 8000

# Main models
The slack_poll app contain four main models as shown in this diagram

![RM diagram](./img/rm-diagram.png)

in words a

1. DailyPoll is the day and name of a poll that was or is going to be polled.
2. Option are a set of words that are in relation to a DailyPoll. A DailyPoll can contain several option (candidates) and a candidate belongs only to one DailyPoll.
3. The Voters are the users that can answer a poll, and choose one of their options.
4. Votes represent the connection between a poll, an option, a voter and a comments that the voter can make to his choice.

# Main views in expected flow
The first and more simple view, its a Django User Login View. Placed at http://localhost:8000/login/

![Login view](./img/login_view.png)

All the CRUD endpoint are placed by a Django RestFramework views. They can be used by a logg

![Rest view](./img/rest_view.png)

Using this endpoints there exists another two custom views, which enables the user to modify a poll, changing it names and options, see the users who had answered the poll their choices and comments

![Pollmanager view](./img/pollmanager_view.png)

when a poll is ready to be executed, the user can send it to all voters by pressing down the "Send poll to all voters" button. This hits the endpoint "send_poll" which asynchronously via a Celery Broker and a Celery Worker retrieve all users currently available on the slack workspace, and send to each of them a message containing the options for the poll and a link to answer the poll

![Slack message](./img/slack-messaging-sample.png)


On the Vote view the user can answer the poll, choosing one of the available options and perform his Vote.

![Vote view](./img/vote_view.png)

If the current is beyond the 11:00AM hard coded limit, the vote page will not update and will show the warning message. "Too late to choose sorry, try tomorrow prior to 11:00AM CL".

![Can't vote view](./img/cantvote_view.png)
