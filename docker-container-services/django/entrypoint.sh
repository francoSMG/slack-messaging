#!/bin/sh

#delete all *.pyc files and migration folders on volume
find . -name "*.pyc" -type f -print0 | xargs -0 /bin/rm -f
find . -name migrations -exec rm -rf {}

#
python manage.py makemigrations slack_poll
python manage.py makemigrations
python manage.py migrate

echo "from django.contrib.auth import get_user_model; User = get_user_model(); User.objects.create_superuser('nora@slack-messaging.com', 'nora@slack-messaging.com', 'password')" | python manage.py shell

exec "$@"
