from base.models import BaseModel
from django.db import models
import uuid

class DailyPoll(BaseModel):
    day = models.DateField(
        null=False, blank=False)
    name = models.TextField(
        default='menu')

    def __str__(self):
        return 'Daily poll :'+self.name+' of '+str(self.day)

    def send_poll_to_slack(self):
        bot_token = "xoxb-1789198362515-1795181382548-eKOzrjWHI9BJAhKMnrHhIv87"
        #list all available users
        #send a message to each user


class Options(BaseModel):
    poll = models.ForeignKey(
        DailyPoll, on_delete=models.CASCADE, null=True)
    option = models.TextField(
        null=False, blank=False)

    def __str__(self):
        return 'Option in poll :'+str(self.poll)+', option: '+str(self.option)


class Voters(BaseModel):
    user_id = models.TextField(
        null=False, blank=False)
    name = models.TextField(
        null=False, blank=False)
    uuid = models.UUIDField(
        default=uuid.uuid4, editable=False, unique=True)

    def __str__(self):
        return 'Voter :'+str(self.uuid)+', name: '+str(self.name)


class Votes(BaseModel):
    poll = models.ForeignKey(
        DailyPoll, on_delete=models.CASCADE)
    choice = models.ForeignKey(
        Options, on_delete=models.CASCADE, null=True)
    voter = models.ForeignKey(
        Voters, on_delete=models.CASCADE)
    comments = models.TextField(
        null=False, blank=False)

    def __str__(self):
        return 'Vote of voter :'+str(self.voter)+', poll: '+str(self.poll)

