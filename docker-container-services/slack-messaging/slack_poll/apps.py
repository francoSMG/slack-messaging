from django.apps import AppConfig


class SlackPollConfig(AppConfig):
    name = 'slack_poll'
