from slack_poll.models import DailyPoll, Options, Voters, Votes
from rest_framework import serializers

class DailyPollSerializer(serializers.ModelSerializer):
    class Meta:
        model = DailyPoll
        fields = ['id','day','name']

class OptionsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Options
        fields = ['id','poll','option']


class VotersSerializer(serializers.ModelSerializer):
    class Meta:
        model = Voters
        fields = ['id','user_id','name']


class VotesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Votes
        fields = ['id','poll','choice','voter','comments']


