from django.contrib import admin
from django.urls import path
from base.urls import base_url_patterns

from slack_poll.views import  DailyPollList, DailyPollRetrieveUpdateDestroyView, OptionsList, OptionsRetrieveUpdateDestroyView, VotersList, VotersRetrieveUpdateDestroyView, VotesList, VotesRetrieveUpdateDestroyView
from slack_poll.views import ManageDailyPollsView, PollSenderView, VotePollView

urlpatterns = base_url_patterns
urlpatterns += [
    # REST framework CRUD endpoints
    path('dailypolls/', DailyPollList.as_view(), name='dailypoll_list'),
    path('dailypolls/<int:pk>', DailyPollRetrieveUpdateDestroyView.as_view(), name='dailypoll_retrieveupdatedestroy'),
    path('options/', OptionsList.as_view(), name='options_list'),
    path('options/<int:pk>', OptionsRetrieveUpdateDestroyView.as_view(), name='options_retrieveupdatedestroy'),
    path('voters/', VotersList.as_view(), name='voters_list'),
    path('voters/<int:pk>', VotersRetrieveUpdateDestroyView.as_view(), name='voters_retrieveupdatedestroy'),
    path('votes/', VotesList.as_view(), name='votes_list'),
    path('votes/<int:pk>', VotesRetrieveUpdateDestroyView.as_view(), name='votes_retrieveupdatedestroy'),
]

pollsenderview = PollSenderView()
urlpatterns += [
    # Custom views
    path('manage_polls', ManageDailyPollsView.as_view(), name='manage_dailypolls' ),
    path('send_poll', pollsenderview.send_poll_view, name='send_poll' ),
    path('vote_poll/<uuid:voter_uuid>/<int:dailypoll_id>/', VotePollView.as_view(), name='vote_poll' )
]
