from django.shortcuts import render
from base.views import BaseListCreateAPIView, BaseRetrieveUpdateDestroyAPIView, BaseTemplateView

from slack_poll.models import DailyPoll, Options, Voters, Votes
from slack_poll.serializers import DailyPollSerializer, OptionsSerializer, VotersSerializer, VotesSerializer

from datetime import date, datetime
from django.utils.dateparse import parse_date

from django.http import HttpResponse, HttpResponseNotFound

from celery.decorators import task

import requests


class DailyPollList(BaseListCreateAPIView):
    queryset = DailyPoll.objects.all()
    serializer_class = DailyPollSerializer

class DailyPollRetrieveUpdateDestroyView(BaseRetrieveUpdateDestroyAPIView):
    queryset = DailyPoll
    serializer_class = DailyPollSerializer

class OptionsList(BaseListCreateAPIView):
    queryset = Options.objects.all()
    serializer_class = OptionsSerializer

class OptionsRetrieveUpdateDestroyView(BaseRetrieveUpdateDestroyAPIView):
    queryset = Options
    serializer_class = OptionsSerializer

class VotersList(BaseListCreateAPIView):
    queryset = Voters.objects.all()
    serializer_class = VotersSerializer

class VotersRetrieveUpdateDestroyView(BaseRetrieveUpdateDestroyAPIView):
    queryset = Voters
    serializer_class = VotersSerializer

class VotesList(BaseListCreateAPIView):
    queryset = Votes.objects.all()
    serializer_class = VotesSerializer

class VotesRetrieveUpdateDestroyView(BaseRetrieveUpdateDestroyAPIView):
    queryset = Votes
    serializer_class = VotesSerializer


class ManageDailyPollsView(BaseTemplateView):
    template_name = 'slack_poll/manage_dailypolls.html'
    day_to_manage = None
    managing_poll = None
    options = None
    votes = None

    def dispatch(self, request, *args, **kwargs):
        self.day_to_manage = request.POST.get('day_to_manage')
        if not self.day_to_manage:
            self.day_to_manage = date.today().strftime("%Y-%m-%d")
        self.date_to_manage = parse_date(self.day_to_manage)

        managing_poll = DailyPoll.objects.filter(day=self.date_to_manage).first()

        if not managing_poll:
            managing_poll = DailyPoll(day=self.date_to_manage)
            managing_poll.save()

        self.managing_poll = managing_poll
        print(self.managing_poll)

        return super().dispatch(request, *args, **kwargs)

    def post(self, request):

        options = Options.objects.filter(poll=self.managing_poll)
        votes = Votes.objects.filter(poll=self.managing_poll)

        self.options = options
        self.votes = votes

        return super().get(request)

    def get_context_data(self,**kwargs):
        context = super().get_context_data(**kwargs)
        context['day_to_manage'] = self.day_to_manage
        context['managing_poll'] = self.managing_poll
        context['options'] = self.options
        context['votes'] = self.votes
        return context


@task(name='send_task')
def send_task(slack_bot_token,slack_send_pm_endpoint,slack_user_id,slack_user_name,message,poll_id):
    voter, created= Voters.objects.get_or_create(
        user_id=slack_user_id,
        name=slack_user_name)
    message_with_link = message + '\n'+'Go to http://localhost:8000/slack_poll/vote_poll/'+str(voter.uuid)+'/'+str(poll_id)
    headers = {'Authorization': "Bearer "+slack_bot_token}
    send_message_endpoint = slack_send_pm_endpoint +'?channel='+voter.user_id+'&text='+message_with_link
    requests.post(send_message_endpoint, headers=headers).json()


class PollSenderView():
    slack_bot_token = 'xoxb-1789198362515-1795181382548-eKOzrjWHI9BJAhKMnrHhIv87'
    slack_users_list_endpoint = 'https://slack.com/api/users.list'
    slack_send_pm_endpoint = 'https://slack.com/api/chat.postMessage'

    def send_poll_view(self,request):
        poll_to_send = request.POST.get('poll_to_send')
        if not poll_to_send:
            return HttpResponseNotFound('need poll_to_send as POST parameter')
        poll_to_send = DailyPoll.objects.filter(id=poll_to_send).first()
        if not poll_to_send:
            return HttpResponseNotFound('not a valid poll')

        self.poll_to_send = poll_to_send

        options = Options.objects.filter(poll=poll_to_send)
        message = ['Hello,for '+poll_to_send.name+' at '+poll_to_send.day.strftime('%d/%m/%Y')+' the choices are:']
        for option in options:
            message.append(option.option)
        message = '\n'.join(message)

        headers = {'Authorization': "Bearer "+self.slack_bot_token}
        user_list = requests.post(self.slack_users_list_endpoint, headers=headers).json()

        valid_users= []
        for user in user_list.get('members'):
            if not user.get('is_bot') or user.get('is_app_user') :
                valid_users.append(
                    {
                        'slack_user_id': user.get('id'),
                        'slack_user_name':user.get('name')
                    })

        for user in valid_users:
            slack_user_id = user.get('slack_user_id')
            slack_user_name = user.get('slack_user_name')
            send_task.delay(
                slack_bot_token=self.slack_bot_token,
                slack_send_pm_endpoint=self.slack_send_pm_endpoint,
                slack_user_id=slack_user_id,
                slack_user_name=slack_user_name,
                message=message,
                poll_id=self.poll_to_send.id)

        return HttpResponse('Sending message to users: '+message)


class VotePollView(BaseTemplateView):
    template_name = 'slack_poll/vote_dailypolls.html'

    def dispatch(self, request, *args, **kwargs ):
        voter_uuid=kwargs.get('voter_uuid')
        dailypoll_id=kwargs.get('dailypoll_id')

        self.voter = Voters.objects.filter(
            uuid=voter_uuid).first()
        self.dailypoll = DailyPoll.objects.filter(
            id=dailypoll_id).first()

        if not self.voter or not self.dailypoll:
            return HttpResponseNotFound('not found polling data')

        self.options = Options.objects.filter(
            poll=self.dailypoll)
        self.vote = Votes.objects.filter(
            poll=self.dailypoll,
            voter=self.voter).first()

        print(self.vote)
        return super().dispatch(request, *args, **kwargs)

    def get(self,request, *args, **kwargs ):
        return super().get(request)

    def post(self, request, *args, **kwargs ):
        option = request.POST.get('choice')
        comments = request.POST.get('comments')

        vote, created = Votes.objects.get_or_create(
            poll=self.dailypoll,
            voter=self.voter)

        if option:
            vote.option = Options.objects.get(id=option)
        vote.comments = comments
        vote.save()
        return super().get(request)

    def get_context_data(self,**kwargs):
        context = super().get_context_data(**kwargs)
        context['voter'] = self.voter
        context['dailypoll'] = self.dailypoll
        context['options'] = self.options
        context['vote'] = self.vote
        context['can_vote'] = datetime.now().hour <11
        return context
