from base.tests import BaseTestCase

from slack_poll.models import DailyPoll, Options, Voters, Votes
from slack_poll.serializers import DailyPollSerializer, OptionsSerializer, VotersSerializer, VotesSerializer

from datetime import date
from django.utils.dateparse import parse_date

class SlackPollTestBase(BaseTestCase):

    def setUp(self):
        self.test_date = parse_date(date.today().strftime("%Y-%m-%d"))
        self.dailypoll = DailyPoll(
            day = self.test_date,
            name = 'test poll')
        self.dailypoll.save()

        self.options = []
        for option_text in ['option1', 'option2']:
            option = Options(
                poll=self.dailypoll,
                option=option_text)
            option.save()
            self.options.append(option)

        self.voters = []
        for voter_name in ['name1', 'name2']:
            voter = Voters(
                user_id=voter_name,
                name=voter_name)
            voter.save()
            self.voters.append(voter)

        self.votes = []
        for vote_option in [0,1]:
            vote = Votes(
                voter=self.voters[vote_option],
                poll=self.dailypoll,
                choice=self.options[vote_option],
                comments='a test comment')
            vote.save()
            self.votes.append(vote)


    def test_assert_data_exists(self):
        self.assertEquals(DailyPoll.objects.all().count(),1)
        self.assertEquals(Options.objects.all().count(),2)
        self.assertEquals(Voters.objects.all().count(),2)
        self.assertEquals(Votes.objects.all().count(),2)

    def test_all_voters_with_uuid(self):
        from uuid import UUID
        for voter in Voters.objects.all():
            try:
                UUID(str(voter.uuid), version=4)
            except ValueError:
                self.assertTrue(False)

    def test_all_voters_with_uuid(self):
        from uuid import UUID
        for voter in Voters.objects.all():
            try:
                UUID(str(voter.uuid), version=4)
            except ValueError:
                self.assertTrue(False)
