from django.contrib import admin
from django.urls import path
from base import views

base_url_patterns = [
    path('ping/', views.pong, name='slack_poll_pong'),
]
