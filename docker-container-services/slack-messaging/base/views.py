from django.shortcuts import render
from django.http import HttpResponse
from rest_framework import generics
from django.views.generic import TemplateView


def pong(request):
    return HttpResponse("pong")


class BaseListCreateAPIView(generics.ListCreateAPIView):
    pass


class BaseRetrieveUpdateDestroyAPIView(generics.RetrieveUpdateDestroyAPIView):
    pass

class BaseTemplateView(TemplateView):
    pass
